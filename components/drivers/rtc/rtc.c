/*
 * File      : rtc.c
 * Change Logs:
 * Date           Author       Notes
 * 2014-12-19     saint       LPC1765 first version.

 */

#include <time.h>
#include <string.h>
#include <rtthread.h>

#include "lpc17xx.h"
#include "rtc.h"




volatile uint32_t alarm_on = 0;

static struct rt_device rtc;


/*****************************************************************************
** Function name:		RTCInit
**
** Descriptions:		Initialize RTC timer
**
** parameters:			None
** Returned value:		None
** 
*****************************************************************************/

void rt_hw_rtc_init(void)
{
  alarm_on = 0;

  /* Enable CLOCK into RTC */
  LPC_SC->PCONP |= (1 << 9);
  /* If RTC is stopped, clear STOP bit. */
  if ( LPC_RTC->RTC_AUX & (0x1<<4) )
  {
	LPC_RTC->RTC_AUX |= (0x1<<4);	
  }
  
  /*--- Initialize registers ---*/ 
  LPC_RTC->ILR = 0;   
  LPC_RTC->AMR = 0xFF;
  LPC_RTC->CIIR = 0;
  LPC_RTC->CCR = 0;
  RTCStart();
  return;
}




/*****************************************************************************
** Function name:		RTCStart
**
** Descriptions:		Start RTC timer
**
** parameters:			None
** Returned value:		None
** 
*****************************************************************************/
void RTCStart( void ) 
{
  /*--- Start RTC counters ---*/
  LPC_RTC->CCR |= CCR_CLKEN;
  LPC_RTC->ILR = ILR_RTCCIF;
  return;
}

/*****************************************************************************
** Function name:		RTCStop
**
** Descriptions:		Stop RTC timer
**
** parameters:			None
** Returned value:		None
** 
*****************************************************************************/
void RTCStop( void )
{   
  /*--- Stop RTC counters ---*/
  LPC_RTC->CCR &= ~CCR_CLKEN;
  return;
} 

/*****************************************************************************
** Function name:		RTC_CTCReset
**
** Descriptions:		Reset RTC clock tick counter
**
** parameters:			None
** Returned value:		None
** 
*****************************************************************************/
void RTC_CTCReset( void )
{   
  /*--- Reset CTC ---*/
  LPC_RTC->CCR |= CCR_CTCRST;
  return;
}

/** \brief set system date(time not modify).
 *
 * \param rt_uint32_t year  e.g: 2012.
 * \param rt_uint32_t month e.g: 12 (1~12).
 * \param rt_uint32_t day   e.g: e.g: 31.
 * \return rt_err_t if set success, return RT_EOK.
 *
 */
rt_err_t set_date(rt_uint32_t year, rt_uint32_t month, rt_uint32_t day)
{
  	LPC_RTC->DOM = day;
  	LPC_RTC->MONTH = month;
  	LPC_RTC->YEAR = year;  

    return 0;
}

/** \brief set system time(date not modify).
 *
 * \param rt_uint32_t hour   e.g: 0~23.
 * \param rt_uint32_t minute e.g: 0~59.
 * \param rt_uint32_t second e.g: 0~59.
 * \return rt_err_t if set success, return RT_EOK.
 *
 */

rt_err_t set_time(rt_uint32_t hour, rt_uint32_t minute, rt_uint32_t second)
{
    LPC_RTC->SEC = second;
  	LPC_RTC->MIN = minute;
  	LPC_RTC->HOUR = hour;

    return 0;
}

#ifdef RT_USING_FINSH
#include <finsh.h>
#include <rtdevice.h>

void list_date(void)
{
    time_t now;

    //now = LPC_RTC->CTIME0;
    rt_kprintf("%d-%d-%d,%d:%d:%d\n", LPC_RTC->YEAR,LPC_RTC->MONTH,LPC_RTC->DOM,LPC_RTC->HOUR,LPC_RTC->MIN,LPC_RTC->SEC);
}
FINSH_FUNCTION_EXPORT(list_date, show date and time.)

FINSH_FUNCTION_EXPORT(set_date, set date. e.g: set_date(2014,12,19))
FINSH_FUNCTION_EXPORT(set_time, set time. e.g: set_time(23,59,59))
#endif
