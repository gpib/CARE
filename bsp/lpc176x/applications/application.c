/*
 * File      : application.c
 * This file is part of RT-Thread RTOS
 * COPYRIGHT (C) 2009, RT-Thread Development Team
 *
 * The license and distribution terms for this file may be
 * found in the file LICENSE in this distribution or at
 * http://www.rt-thread.org/license/LICENSE
 *
 * Change Logs:
 * Date           Author       Notes
 * 2009-01-05     Bernard      the first version
 * 2010-03-04     Magicoe      for LPC1766 version
 * 2010-05-02     Aozima       add led function
 * 2010-05-24     Bernard      add filesystem initialization and move led function to led.c
 * 2012-04-15     Bernard      enable components_init.
 */


#include <rtthread.h>
#include "platform.h"
#include <string.h>


#ifdef RT_USING_LWIP
#include <emac.h>
#include <netif/ethernetif.h>
extern int lwip_system_init(void);
#endif

#ifdef RT_USING_DFS
#include <dfs.h>
#include <dfs_posix.h>
#include <sd.h>
#include <dfs_init.h>
#include <dfs_fs.h>
#include <dfs_elm.h>
#endif


#include "care.h"
#include "led.h"
#include "GPIB.h"
#include "rtc.h"
#include "IAP\IAP.h"
#include "timer_control.h"
#include "ring.h"

#include "usb\cdcuser.h"
#include "usb\usbhw.h"
#include "usb\usb.h"
#include "usb\vcom.h"

#include "network\tcpserver.h"
#include "network\udpserver.h"
//#include "network\tcpclient.h"


//extern int strpos (const char *s, char c);

ALIGN(RT_ALIGN_SIZE)

static rt_uint8_t led_stack[ 512 ];
static struct rt_thread led_thread;


static rt_uint8_t TCPSERV_stack[ 1536 ];
static struct rt_thread TCPSERV_thread;
static rt_uint16_t sw_count=0;

//static rt_uint8_t UDPSERV_stack[ 512 ];
//static struct rt_thread UDPSERV_thread;

//static rt_uint8_t GPIB_S_stack[ 512 ];
//static struct rt_thread GPIB_S_thread;

//static rt_uint8_t GPIB_R_stack[ 512 ];
//static struct rt_thread GPIB_R_thread;


static char ln_buf[128],tmp_buf[128]; 
//static	rt_uint8_t key_rest=0;



rt_uint8_t g_flagUsbEvent = 0;

#define USB_EVENT_MAX 16
rt_uint32_t g_eventQueue[USB_EVENT_MAX];
rt_uint32_t g_eventQueueLen = 0;

rt_uint32_t usbEventGet(rt_uint16_t * EPNum, rt_uint16_t * event)
{
    rt_uint32_t i;

    NVIC_DisableIRQ(USB_IRQn);               /* disable USB interrupt */

    if (g_eventQueueLen == 0)
    {
        NVIC_EnableIRQ(USB_IRQn);               /* enable USB interrupt */
        return 0;
    }

    *EPNum = (rt_uint16_t)(g_eventQueue[0] >> 16);
    *event = (rt_uint16_t)g_eventQueue[0];

    g_eventQueueLen--;

    for (i = 0; i < g_eventQueueLen; i++)
    {
        g_eventQueue[i] = g_eventQueue[i+1];
    }

    NVIC_EnableIRQ(USB_IRQn);               /* enable USB interrupt */
    return 1;
}

void usbEventCb(rt_uint16_t EPNum, rt_uint16_t event)
{
    if (g_eventQueueLen >= USB_EVENT_MAX)
    {
        return;
    }

    g_eventQueue[g_eventQueueLen] = (rt_uint32_t)EPNum << 16 | (rt_uint32_t)event ;

    g_eventQueueLen++;

    g_flagUsbEvent = 1;

}

//static 
void offline_read_setting()
{ //文件系统测试代码 
	int fd=0,str_pos=0;
	rt_uint8_t i=0,count=1,read_success=0;	
	char buf_str[128];
	
	rt_memset(ln_buf,0,128); 
	if (PIN0_IS_CLR(SD_INSERT))
	{
		fd = open("/SETTING.txt",DFS_O_RDONLY,0);	
		if(fd >= 0)
		{
			while ((count != 0)&&(i<MAX_OFFLINE_CMD))
			{
				read_success = 0;
				rt_memset(ln_buf,0,128); 
				count = readln(ln_buf); 
				if ((count>0)&&(ln_buf[0]!=0x0D)&&(ln_buf[1]!=0x0A))
				{
					str_pos = 0;
					rt_memset(tmp_buf,0,128); 
					if (strcpy(tmp_buf,strstr(ln_buf,"command")))
						str_pos = strchr(tmp_buf,',') - tmp_buf;
					if (str_pos>9) {strncpy(offline_str[i].cmd,tmp_buf+9,str_pos-10);read_success+=1;}					
					
					str_pos = 0;
					rt_memset(tmp_buf,0,128); 
					if (strcpy(tmp_buf,strstr(ln_buf,"filename")))//COPY从filename开始到结束的字符
					{
						str_pos = strchr(tmp_buf,';') - tmp_buf;
						if (str_pos>9) {strncpy(offline_str[i].file_name,tmp_buf+9,str_pos-9);read_success+=1;}	
					}
						
					str_pos = 0;
					rt_memset(tmp_buf,0,128); 
					memset(buf_str, 0, 128);
					if (strcpy(tmp_buf,strstr(ln_buf,"time")))
						str_pos = strchr(tmp_buf,',') - tmp_buf;
					if (str_pos>5) 
					{
						strncpy(buf_str,tmp_buf+5,str_pos-5);					
						offline_str[i].period = StrHex(buf_str);
						read_success+=1;
					}
						
					str_pos = 0;
					rt_memset(tmp_buf,0,128); 
					memset(buf_str, 0, 128);
					if (strcpy(tmp_buf,strstr(ln_buf,"address")))
						str_pos = strchr(tmp_buf,',') - tmp_buf;
					if (str_pos>8) 
					{
						strncpy(buf_str,tmp_buf+8,str_pos-8);
						offline_str[i].address = StrHex(buf_str)&0xFF;
						read_success+=1;
					}
					if (read_success==4)i++;				
				}
			}
			close(fd); 
			fd =0;			
		}
	}		
		
}


static void offline_entry(void* parameter)
{   	
	offline_LED=0;	
	while (1)//(!UART_Auto_Mode)
	{	

		SW_COUNT_GET(sw_count);
		
		if (PIN0_IS_SET(SW2))//按键弹起
		{
			if ((!offline_start)&&PIN0_IS_CLR(SD_INSERT)&&(sw_count>= 4000))//如果插入了TF卡以及离线采集没有启动
			{
				offline_start=1; 
				read_once=0;
				sw_count = 0;
				SW_COUNT_SET(sw_count);
			}
			
			if((sw_count < 1500)&&(sw_count > 100)&&(offline_start))//停止离线采集
			{
				NVIC_SystemReset();
				offline_start=0;
				sw_count = 0;
				SW_COUNT_SET(sw_count);
				offline_return=0;	
			}
			
			if((sw_count < 1500)&&(sw_count > 100)&&(!offline_start)&&(!UART_Auto_Mode)) //短按启动listen only模式
			{
				Listen_Only_Mode = ~Listen_Only_Mode;
				sw_count = 0;
				SW_COUNT_SET(sw_count);
			}
			
			if((sw_count < 1500)&&(sw_count > 100)&&(UART_Auto_Mode))//停止wifi端口透传模式
			{
				UART_Auto_Mode=0;
				WriteFlash(0);
				sw_count = 0;
				SW_COUNT_SET(sw_count);
				NVIC_SystemReset();
			}
			
		}

		if ((PIN0_IS_CLR(SD_INSERT))&&(read_once==1)){read_once=0;NVIC_SystemReset();}				//插入TF卡重启CARE
		
		if (PIN0_IS_SET(SD_INSERT)) {read_once=1;	offline_start=0;} 													//拔卡停止采集，再次启动离线采集重新读取TF配置文件
			
		if (offline_start){offline_collect();}																								//启动离线采集
		
		if (Listen_Only_Mode) gpib_read(1,1,0,0);																							//启动TALK ONLY模式
	
		//serial_poll;
		
		rt_thread_delay(10);
	}
}



static void led_thread_entry(void* parameter)
{   	

	rt_hw_led_init();
	LPC_GPIO0->FIODIR &= ~BV(SW1);
	LPC_GPIO0->FIODIR &= ~BV(SW2);
	LPC_GPIO0->FIODIR &= ~BV(SD_INSERT);
	LPC_GPIO0->FIODIR &= ~BV(nSUSPEND);
	
	LPC_GPIO2->FIODIR |= BV(GPIO_5);
	LPC_GPIO0->FIODIR |= BV(GPIO_6);
	LPC_GPIO0->FIODIR |= BV(GPIO_7);
	LPC_GPIO0->FIODIR |= BV(GPIO_8);
	
	PIN2_SET(GPIO_5);
	PIN0_SET(GPIO_6);
	PIN0_SET(GPIO_7);
	PIN0_SET(GPIO_8);


	while (1)
	{

		if(!offline_start){if (PIN0_IS_CLR(SD_INSERT)) rt_hw_led_on(0); else rt_hw_led_off(0);}
		rt_hw_led_on(1);
		
		if(!UART_Auto_Mode)
			rt_thread_delay( RT_TICK_PER_SECOND/2 ); /* sleep 0.5 second and switch to other thread */
		else
			rt_thread_delay( RT_TICK_PER_SECOND/10 );

		if (PIN0_IS_SET(nSUSPEND)||UART_Auto_Mode) rt_hw_led_off(1);
		
		if(!UART_Auto_Mode)
			rt_thread_delay( RT_TICK_PER_SECOND/2 ); /* sleep 0.5 second and switch to other thread */
		else
			rt_thread_delay( RT_TICK_PER_SECOND/10 );
	}
}

/* thread phase init */
void rt_init_thread_entry(void *parameter)
{
	/* initialize platform */
	u32IAP_ReadSerialNumber(&LPC1768_NO[0],&LPC1768_NO[1],&LPC1768_NO[2],&LPC1768_NO[3]);	//mac address
	ReadFlash();																																	//configuration parameter

	platform_init();

	rt_hw_rtc_init();	 	//初始化RTC
	
#ifdef RT_USING_LWIP
  /* register Ethernet interface device */
  lpc17xx_emac_hw_init();

  /* initialize lwip stack */
	/* register ethernetif device */
	eth_system_device_init();

	/* initialize lwip system */
	lwip_system_init();
//	rt_kprintf("TCP/IP initialized!\n");
#endif

   /* Filesystem Initialization */
#ifdef RT_USING_DFS
  rt_hw_sdcard_init();

	/* initialize the device file system */
	dfs_init();

#ifdef RT_USING_DFS_ELMFAT
	/* initialize the elm chan FatFS file system*/
	elm_init();
#endif

	/* mount sd card fat partition 1 as root directory */
	dfs_mount("sd0", "/", "elm", 0, 0);
//	if (dfs_mount("sd0", "/", "elm", 0, 0) == 0)
//		rt_kprintf("File System initialized!\n");
//	else
//		rt_kprintf("File System init failed!\n");
#endif

CDC_Init(0);
USB_Init( usbEventCb );				/* USB Initialization */
USB_Connect(1);		                /* USB Connect */  

	timer_control_init();
	care_GPIB_init();
	
	read_once=0;	
	offline_start=0;
	offline_read_setting();
	
	Listen_Only_Mode = 0;
//		ring_buf_init();
// 		rt_usb_device_init();
}

int rt_application_init()
{
    rt_thread_t tid;
		rt_err_t result;
	

    tid = rt_thread_create("init",
    		rt_init_thread_entry, RT_NULL,
    		2048, RT_THREAD_PRIORITY_MAX/3, 20);
    if (tid != RT_NULL) rt_thread_startup(tid);
	

	/* init led thread */
	result = rt_thread_init(&led_thread,"led",led_thread_entry, RT_NULL,(rt_uint8_t*)&led_stack[0], sizeof(led_stack), 25, 10);

	if (result == RT_EOK)
	{
        rt_thread_startup(&led_thread);
	}
	

		result = rt_thread_init(&TCPSERV_thread,
		"tcpserv",
		tcpserv, RT_NULL,
		(rt_uint8_t*)&TCPSERV_stack[0], sizeof(TCPSERV_stack), 20, 20);

		if (result == RT_EOK)
		{
				rt_thread_startup(&TCPSERV_thread);
		}

//		result = rt_thread_init(&UDPSERV_thread,
//		"udpserv",
//		udpserv, RT_NULL,
//		(rt_uint8_t*)&UDPSERV_stack[0], sizeof(UDPSERV_stack), 20, 50);

//		if (result == RT_EOK)
//		{
//				rt_thread_startup(&UDPSERV_thread);
//		}
		
		tid = rt_thread_create("offline", offline_entry, RT_NULL, 1536, 20, 20);
    if (tid != RT_NULL) rt_thread_startup(tid);
		
    return 0;
}
