#include <rtthread.h>
static unsigned int _seed=1;
#define M   ((1U<<31) -1)
#define A   4871
#define Q   4488        // M/A
#define R   3399        // M%A; R < Q !!!
int rand_r(unsigned int* seed)
{
    rt_int32_t X;
    X = *seed;
    X = A*(X%Q) - R * (rt_int32_t) (X/Q);
    if (X < 0)
    {
        X += M;
    }
    *seed = X;
    return X;
}
int rand(void)
{
    return rand_r(&_seed);
}
void srand(unsigned int i)
{
    _seed=i;
}
