#ifndef __UART2_H__
#define __UART2_H__
void rt_hw_uart2_init(void);
void uart2_thread_entry(void* parameter);
#endif
