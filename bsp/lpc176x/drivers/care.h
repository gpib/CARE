#ifndef __CARE_H__
#define __CARE_H__
#include "LPC17xx.h"
#include <rtthread.h>
#define     SD_INSERT           19          /* LPC_GPIO0 */
#define     SW1                     24          /* LPC_GPIO0 */
#define     SW2                     25          /* LPC_GPIO0 */
#define     LED1                    7               /* LPC_GPIO2 */
#define     LED2                    23          /* LPC_GPIO0 */ //SWITCH
#define     nSUSPEND            26          /* LPC_GPIO0 */ //usb nSUSPEND
#define     TX_DHT              10          /* LPC_GPIO0 */
#define     RX_DHT              11          /* LPC_GPIO0 */
/*---------------WIFI端口四个----------------*/
#define     WIFI_RESET      5               /* LPC_GPIO2 *///OUT
#define     WIFI_LINK           4               /* LPC_GPIO2 *///IN
#define     WIFI_nCTS           3               /* LPC_GPIO2 *///主机接收使能信号，低电平有效   OUT
#define     WIFI_nRTS           2               /* LPC_GPIO2 *///模块接收使能信号，低电平有效 IN
/*---------------扩展端口四个----------------*/
#define     GPIO_5              8               /* LPC_GPIO2 */
#define     GPIO_6              20          /* LPC_GPIO0 */
#define     GPIO_7              21          /* LPC_GPIO0 */
#define     GPIO_8              22          /* LPC_GPIO0 */
/*---------------CP2102功能端口-----------------*/
#define     CP2102_DSR      28          /* LPC_GPIO4 *///Data Set Ready control input (active low)
#define     CP2102_CTS      6               /* LPC_GPIO0 *///Clear To Send control input (active low)
#define     CP2102_DCD      7               /* LPC_GPIO0 *///Data Carrier Detect control input (active low)
#define     CP2102_RI           8               /* LPC_GPIO0 *///Ring Indicator control input (active low)
#define     CP2102_RTS      4               /* LPC_GPIO0 *///Ready to Send control output (active low)
#define     CP2102_DTR      5               /* LPC_GPIO0 *///Data Terminal Ready control output (active low)
/*-----------------DATA----------------*/
#define     DIO1        25          /* LPC_GPIO3 */
#define     DIO2        19          /* LPC_GPIO1 */
#define     DIO3        21          /* LPC_GPIO1 */
#define     DIO4        23          /* LPC_GPIO1 */
#define     DIO5        26          /* LPC_GPIO3 */
#define     DIO6        18          /* LPC_GPIO1 */
#define     DIO7        20          /* LPC_GPIO1 */
#define     DIO8        22          /* LPC_GPIO1 */
/*-----------------CONTROL-----------*/
#define     NRFD        0               /* LPC_GPIO0 */
#define     NDAC        1               /* LPC_GPIO0 */
#define     ATN         27          /* LPC_GPIO1 */
#define     SRQ         26          /* LPC_GPIO1 */
#define     EOI         28          /* LPC_GPIO1 */
#define     DAV         29          /* LPC_GPIO1 */
#define     REN         12          /* LPC_GPIO2 */
#define     IFC         13          /* LPC_GPIO2 */
#define NRFD_LOW()      TE_LOW();       LPC_GPIO0->FIODIR |= BV(NRFD);  LPC_GPIO0->FIOCLR |= BV(NRFD)
#define NDAC_LOW()      TE_LOW();       LPC_GPIO0->FIODIR |= BV(NDAC);  LPC_GPIO0->FIOCLR |= BV(NDAC)
#define ATN_LOW()       DC_LOW();       LPC_GPIO1->FIODIR |= BV(ATN);       LPC_GPIO1->FIOCLR |= BV(ATN)
#define SRQ_LOW()       DC_HIGH();  LPC_GPIO1->FIODIR |= BV(SRQ);       LPC_GPIO1->FIOCLR |= BV(SRQ)
#define EOI_LOW()       TE_HIGH();  LPC_GPIO1->FIODIR |= BV(EOI);       LPC_GPIO1->FIOCLR |= BV(EOI)       ///////待定
#define DAV_LOW()       TE_HIGH();  LPC_GPIO1->FIODIR |= BV(DAV);       LPC_GPIO1->FIOCLR |= BV(DAV)
#define REN_LOW()       DC_LOW();       LPC_GPIO2->FIODIR |= BV(REN);       LPC_GPIO2->FIOCLR |= BV(REN)
#define IFC_LOW()       DC_LOW();       LPC_GPIO2->FIODIR |= BV(IFC);       LPC_GPIO2->FIOCLR |= BV(IFC)
#define NRFD_HIGH()   TE_LOW();     LPC_GPIO0->FIODIR |= BV(NRFD);  LPC_GPIO0->FIOSET |= BV(NRFD)
#define NDAC_HIGH()   TE_LOW();     LPC_GPIO0->FIODIR |= BV(NDAC);  LPC_GPIO0->FIOSET |= BV(NDAC)
#define ATN_HIGH()      DC_LOW();       LPC_GPIO1->FIODIR |= BV(ATN);       LPC_GPIO1->FIOSET |= BV(ATN)
#define SRQ_HIGH()      DC_HIGH();  LPC_GPIO1->FIODIR |= BV(SRQ);       LPC_GPIO1->FIOSET |= BV(SRQ)
#define EOI_HIGH()      TE_HIGH();  LPC_GPIO1->FIODIR |= BV(EOI);       LPC_GPIO1->FIOSET |= BV(EOI)                ///////待定
#define DAV_HIGH()      TE_HIGH();  LPC_GPIO1->FIODIR |= BV(DAV);       LPC_GPIO1->FIOSET |= BV(DAV)
#define REN_HIGH()      DC_LOW();       LPC_GPIO2->FIODIR |= BV(REN);       LPC_GPIO2->FIOSET |= BV(REN)
#define IFC_HIGH()      DC_LOW();       LPC_GPIO2->FIODIR |= BV(IFC);       LPC_GPIO2->FIOSET |= BV(IFC)
/*  75SN160 75SN162 control                          */
#define     PE      27                  /* LPC_GPIO0 */
#define     TE      24                  /* LPC_GPIO1 */
#define     DC      25                  /* LPC_GPIO1 */
#define     SC      11                  /* LPC_GPIO2 */
#define PE_LOW()   LPC_GPIO0->FIODIR |= BV(PE);LPC_GPIO0->FIOCLR |= BV(PE)
#define TE_LOW()   LPC_GPIO1->FIODIR |= BV(TE);LPC_GPIO1->FIOCLR |= BV(TE)
#define DC_LOW()   LPC_GPIO1->FIODIR |= BV(DC);LPC_GPIO1->FIOCLR |= BV(DC)
#define SC_LOW()   LPC_GPIO2->FIODIR |= BV(SC);LPC_GPIO2->FIOCLR |= BV(SC)
#define PE_HIGH()   LPC_GPIO0->FIODIR |= BV(PE);LPC_GPIO0->FIOSET |= BV(PE)
#define TE_HIGH()   LPC_GPIO1->FIODIR |= BV(TE);LPC_GPIO1->FIOSET |= BV(TE)
#define DC_HIGH()   LPC_GPIO1->FIODIR |= BV(DC);LPC_GPIO1->FIOSET |= BV(DC)
#define SC_HIGH()   LPC_GPIO2->FIODIR |= BV(SC);LPC_GPIO2->FIOSET |= BV(SC)
#define     BV(rt_uint8_t)                                          (1 << (rt_uint8_t))
#define     BIT_SET(rt_uint32_t, rt_uint8_t)        ((rt_uint32_t) |= BV(rt_uint8_t))
#define     BIT_CLEAR(rt_uint32_t, rt_uint8_t)  ((rt_uint32_t) &=!BV(rt_uint8_t))
#define     BIT_IS_SET(rt_uint32_t, rt_uint8_t) (rt_uint32_t & BV(rt_uint8_t))
#define     BIT_IS_CLR(rt_uint32_t, rt_uint8_t) (!(rt_uint32_t & BV(rt_uint8_t)))
#define     LOOP_UNTIL_BIT_IS_SET(rt_uint32_t, rt_uint8_t)          do { } while (BIT_IS_CLEAR(rt_uint32_t, rt_uint8_t))
#define     LOOP_UNTIL_BIT_IS_CLEAR(rt_uint32_t, rt_uint8_t)        do { } while (BIT_IS_SET(rt_uint32_t, rt_uint8_t))
#define     PIN0_SET(rt_uint8_t)                (LPC_GPIO0->FIOSET |= BV(rt_uint8_t))
#define     PIN0_CLR(rt_uint8_t)                (LPC_GPIO0->FIOCLR |= BV(rt_uint8_t))
#define     PIN1_SET(rt_uint8_t)                (LPC_GPIO1->FIOSET |= BV(rt_uint8_t))
#define     PIN1_CLR(rt_uint8_t)                (LPC_GPIO1->FIOCLR |= BV(rt_uint8_t))
#define     PIN2_SET(rt_uint8_t)                (LPC_GPIO2->FIOSET |= BV(rt_uint8_t))
#define     PIN2_CLR(rt_uint8_t)                (LPC_GPIO2->FIOCLR |= BV(rt_uint8_t))
#define     PIN3_SET(rt_uint8_t)                (LPC_GPIO3->FIOSET |= BV(rt_uint8_t))
#define     PIN3_CLR(rt_uint8_t)                (LPC_GPIO3->FIOCLR |= BV(rt_uint8_t))
#define     PIN4_SET(rt_uint8_t)                (LPC_GPIO4->FIOSET |= BV(rt_uint8_t))
#define     PIN4_CLR(rt_uint8_t)                (LPC_GPIO4->FIOCLR |= BV(rt_uint8_t))

#define     PIN0_IS_SET(rt_uint8_t)         ((LPC_GPIO0->FIOPIN >>(rt_uint8_t)) & 1)
#define     PIN1_IS_SET(rt_uint8_t)         ((LPC_GPIO1->FIOPIN >>(rt_uint8_t)) & 1)
#define     PIN2_IS_SET(rt_uint8_t)         ((LPC_GPIO2->FIOPIN >>(rt_uint8_t)) & 1)
#define     PIN3_IS_SET(rt_uint8_t)         ((LPC_GPIO3->FIOPIN >>(rt_uint8_t)) & 1)
#define     PIN4_IS_SET(rt_uint8_t)         ((LPC_GPIO4->FIOPIN >>(rt_uint8_t)) & 1)
#define     PIN0_IS_CLR(rt_uint8_t)         !((LPC_GPIO0->FIOPIN >>(rt_uint8_t)) & 1)
#define     PIN1_IS_CLR(rt_uint8_t)         !((LPC_GPIO1->FIOPIN >>(rt_uint8_t)) & 1)
#define     PIN2_IS_CLR(rt_uint8_t)         !((LPC_GPIO2->FIOPIN >>(rt_uint8_t)) & 1)
#define     PIN3_IS_CLR(rt_uint8_t)         !((LPC_GPIO3->FIOPIN >>(rt_uint8_t)) & 1)
#define     PIN4_IS_CLR(rt_uint8_t)         !((LPC_GPIO4->FIOPIN >>(rt_uint8_t)) & 1)
#define     SW_COUNT_SET(rt_uint16_t)           (timer_sw_count = (rt_uint16_t))
#define     SW_COUNT_GET(rt_uint16_t)           ((rt_uint16_t) = timer_sw_count)
#endif
