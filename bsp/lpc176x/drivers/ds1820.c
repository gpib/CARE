#include "care.h"
void delay(rt_uint32_t i)
{
    while(i--)
    {
        ;    //i=100 4us
    }
}
//初始化函数
rt_uint8_t Init_DS18B20(void)
{
    rt_uint8_t n;
    rt_uint16_t count;
    count = 0;
    LPC_GPIO0->FIODIR |= BV(RX_DHT);
    PIN0_CLR(RX_DHT);
    PIN0_SET(RX_DHT);                                           //复位
    delay(100);                                                         //稍做延时
    PIN0_CLR(RX_DHT);                                           //单片机将DQ拉低
    delay(15000);                                                   //精确延时 大于 480us
    PIN0_SET(RX_DHT);                                           //拉高总线
    delay(500);
    LPC_GPIO0->FIODIR &= ~BV(RX_DHT);
    delay(1500);
    n = PIN0_IS_SET(RX_DHT);
    while (!n)
    {
        n = PIN0_IS_SET(RX_DHT);                    //稍做延时后 如果n=0则初始化成功 n=1则初始化失败
        if (count++>=0xFFF)
        {
            break;
        }
    }
    return n;
}
rt_uint8_t ReadOneChar(void)
{
    rt_uint8_t i=0;
    rt_uint8_t dat = 0;
    for (i=8; i>0; i--)
    {
        LPC_GPIO0->FIODIR |= BV(RX_DHT);
        PIN0_CLR(RX_DHT);
        delay(50);
        dat>>=1;
//          PIN0_SET(RX_DHT);                                    // 给脉冲信号
        LPC_GPIO0->FIODIR &= ~BV(RX_DHT);
        delay(300);
        if(PIN0_IS_SET(RX_DHT))
        {
            dat|=0x80;
        }
        delay(500);
    }
    delay(700);
    return(dat);
}
void WriteOneChar(rt_uint8_t dat)
{
    rt_uint8_t i=0;
    for (i=8; i>0; i--)
    {
        LPC_GPIO0->FIODIR |= BV(RX_DHT);
        PIN0_CLR(RX_DHT);
        delay(100);
        if (dat&0x01)
        {
            PIN0_SET(RX_DHT);
        }
        else
        {
            PIN0_CLR(RX_DHT);
        }
        delay(1100);
        PIN0_SET(RX_DHT);
        delay(100);
        dat>>=1;
    }
    delay(500);
}
rt_uint16_t ReadTemperature(void)
{
    rt_uint8_t a=0;
    rt_uint8_t b=0;
    rt_uint16_t t=0;
    if (Init_DS18B20())
    {
        WriteOneChar(0xCC);                         // 跳过读序号列号的操作
        WriteOneChar(0x44);                         // 启动温度转换
    }
    if (Init_DS18B20())
    {
        WriteOneChar(0xCC);                         //跳过读序号列号的操作
        WriteOneChar(0xBE);                         //读取温度寄存器等（共可读9个寄存器） 前两个就是温度
        a=ReadOneChar();                            //读取温度值低位
        b=ReadOneChar();                            //读取温度值高位
    }
//  a=a>>4;                                 //低位右移4位，舍弃小数部分
//  t=b<<4;                                 //高位左移4位，舍弃符号位
    t=(b<<8)|a;
    return(t);
}
