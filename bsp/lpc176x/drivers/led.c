#include "care.h"
void rt_hw_led_init(void)
{
    LPC_GPIO2->FIODIR |= BV(LED1);  /* led1:P2.7  */
    LPC_GPIO0->FIODIR |= BV(LED2);  /* led2:P0.23 */
}
void rt_hw_led_on(rt_uint32_t led)
{
    switch(led)
    {
    case 0: /* P2.7 = 1 */
        PIN2_SET(LED1);
        break;
    case 1: /* P0.23 = 1 */
        PIN0_SET(LED2);
        break;
    default:
        break;
    }
}
void rt_hw_led_off(rt_uint32_t led)
{
    switch(led)
    {
    case 0: /* P2.7 = 0 */
        PIN2_CLR(LED1);
        break;
    case 1: /* P0.23 = 0 */
        PIN0_CLR(LED2);
        break;
    default:
        break;
    }
}
