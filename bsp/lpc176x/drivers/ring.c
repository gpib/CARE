#include <rtthread.h>
#include <string.h>
#include "ring.h"
#include "GPIB.h"
struct rb
{
    rt_uint16_t read_index, write_index;
    rt_uint8_t *buffer_ptr;
    rt_uint16_t buffer_size;
};
/* 环形buffer的内存块（用数组体现出来�?*/
#define BUFFER_SIZE        128
static rt_uint8_t working_buffer[BUFFER_SIZE];
struct rb working_rb;
static char rx_buffer[128];
rt_uint8_t rx_len=0;
/* 初始化环形buffer，size指的是buffer的大小。注：这里并没对数据地址对齐做处�?*/
void rb_init(rt_uint8_t *pool, rt_uint16_t size)
{
//    RT_ASSERT(working_rb != RT_NULL);
    /* 对读写指针清�?/
    working_rb.read_index = working_rb.write_index = 0;
    /* 设置环形buffer的内存数据块 */
    working_rb.buffer_ptr = pool;
    working_rb.buffer_size = size;
}
/* 向环形buffer中写入数�?*/
rt_bool_t rb_put(const rt_uint8_t *ptr, rt_uint16_t length)
{
    rt_size_t size;
    /* 判断是否有足够的剩余空间 */
    if (working_rb.read_index > working_rb.write_index)
    {
        size = working_rb.read_index - working_rb.write_index;
    }
    else
    {
        size = working_rb.buffer_size - working_rb.write_index + working_rb.read_index;
    }
    /* 没有多余的空�?*/
    if (size < length)
    {
        return RT_FALSE;
    }
    if (working_rb.read_index > working_rb.write_index)
    {
        /* read_index - write_index 即为总的空余空间 */
        memcpy(&working_rb.buffer_ptr[working_rb.write_index], ptr, length);
        working_rb.write_index += length;
    }
    else
    {
        if (working_rb.buffer_size - working_rb.write_index > length)
        {
            /* write_index 后面剩余的空间有足够的长�?*/
            memcpy(&working_rb.buffer_ptr[working_rb.write_index], ptr, length);
            working_rb.write_index += length;
        }
        else
        {
            /*
             * write_index 后面剩余的空间不存在足够的长度，需要把部分数据复制�?
             * 前面的剩余空间中
             */
            memcpy(&working_rb.buffer_ptr[working_rb.write_index], ptr,
                   working_rb.buffer_size - working_rb.write_index);
            memcpy(&working_rb.buffer_ptr[0], &ptr[working_rb.buffer_size - working_rb.write_index],
                   length - (working_rb.buffer_size - working_rb.write_index));
            working_rb.write_index = length - (working_rb.buffer_size - working_rb.write_index);
        }
    }
    return RT_TRUE;
}
/* 从环形buffer中读出数�?*/
rt_bool_t rb_get(rt_uint8_t *ptr, rt_uint16_t length)
{
    rt_size_t size;
    /* 判断是否有足够的数据 */
    if (working_rb.read_index > working_rb.write_index)
    {
        size = working_rb.buffer_size - working_rb.read_index + working_rb.write_index;
    }
    else
    {
        size = working_rb.write_index - working_rb.read_index;
    }
    /* 没有足够的数�?*/
    if (size < length)
    {
        return RT_FALSE;
    }
    if (working_rb.read_index > working_rb.write_index)
    {
        if (working_rb.buffer_size - working_rb.read_index > length)
        {
            /* read_index的数据足够多，直接复�?*/
            memcpy(ptr, &working_rb.buffer_ptr[working_rb.read_index], length);
            working_rb.read_index += length;
        }
        else
        {
            /* read_index的数据不够，需要分段复�?*/
            memcpy(ptr, &working_rb.buffer_ptr[working_rb.read_index],
                   working_rb.buffer_size - working_rb.read_index);
            memcpy(&ptr[working_rb.buffer_size - working_rb.read_index], &working_rb.buffer_ptr[0],
                   length - working_rb.buffer_size + working_rb.read_index);
            working_rb.read_index = length - working_rb.buffer_size + working_rb.read_index;
        }
    }
    else
    {
        /*
         * read_index要比write_index小，总的数据量够（前面已经有总数据量的判
         * 断），直接复制出数据�?
         */
        memcpy(ptr, &working_rb.buffer_ptr[working_rb.read_index], length);
        working_rb.read_index += length;
    }
    return RT_TRUE;
}
void ring_buf_init(void)
{
    rb_init(working_buffer, BUFFER_SIZE);
}
void worker_entry(void* parameter)
{
    rt_uint8_t ch;
    while (1)
    {
        if (rb_get(&ch, 1))
        {
            //////////////////////////////////////
            rx_buffer[rx_len] = ch;
            rx_len++;
            if(rx_buffer[2] == 0x08)
            {
                if ((rx_len-2 == rx_buffer[4] + 3)&&(rx_buffer[4] != 0))        //08 16 07 AA 00 31 32 33 34 35
                {
                    if (care_protocol_state())
                    {
                        care_protocol(rx_buffer[0],rx_buffer+2,rx_len-2);    //GPIB
                    }
                    memset(rx_buffer, 0, 128);
                    rx_len = 0;
                }
            }
            else if((rx_buffer[1+rx_buffer[1]] == 0x0A)&&(rx_buffer[1] != 0x00))
            {
                //      if (uart0_rx_buffer[0] >= 32)
                if (care_protocol_state())
                {
                    care_protocol(rx_buffer[0],rx_buffer+2,rx_len-2);    //GPIB
                }
                memset(rx_buffer, 0, 128);
                rx_len = 0;
            }
            else if(rx_len >= 127)
            {
                memset(rx_buffer, 0, 128);
                rx_len = 0;
            }
        }
//////////////////////////////////////
//          rt_thread_delay(1);
    }
}
