/*
 * File      : board.c
 * This file is part of RT-Thread RTOS
 * COPYRIGHT (C) 2009 RT-Thread Develop Team
 *
 * The license and distribution terms for this file may be
 * found in the file LICENSE in this distribution or at
 * http://www.rt-thread.org/license/LICENSE
 *
 * Change Logs:
 * Date           Author       Notes
 * 2010-03-08     Bernard      The first version for LPC17xx
 * 2010-05-02     Aozima       update CMSIS to 130
 */
#include <rthw.h>
#include <rtthread.h>
#include "LPC17xx.h"
#include "uart2.h"
#include "GPIB.h"
#define IER_RBR     0x01
#define IER_THRE    0x02
#define IER_RLS     0x04
#define IIR_PEND    0x01
#define IIR_RLS     0x03
#define IIR_RDA     0x02
#define IIR_CTI     0x06
#define IIR_THRE    0x01
#define LSR_RDR     0x01
#define LSR_OE      0x02
#define LSR_PE      0x04
#define LSR_FE      0x08
#define LSR_BI      0x10
#define LSR_THRE    0x20
#define LSR_TEMT    0x40
#define LSR_RXFE    0x80
/**
 * @addtogroup LPC11xx
 */
/*@{*/
#if defined(RT_USING_UART2) && defined(RT_USING_DEVICE)
#define UART_BAUDRATE   9600
#define LPC_UART        LPC_UART2
#define UART_IRQn       UART2_IRQn
struct rt_uart_lpc
{
    struct rt_device parent;
    /* buffer for reception */
    rt_uint8_t read_index, save_index;
    rt_uint8_t rx_buffer[RT_UART_RX_BUFFER_SIZE];
} uart2_device;
/* UART接收消息*/
struct rt_semaphore rx2_sem;
/* 接收线程的接收缓冲区*/
static char uart2_rx_buffer[256];
void UART2_IRQHandler(void)
{
    rt_ubase_t level, iir;
    struct rt_uart_lpc* uart = &uart2_device;
    /* read IIR and clear it */
    iir = LPC_UART->IIR;
    iir >>= 1;              /* skip pending bit in IIR */
    iir &= 0x07;            /* check bit 1~3, interrupt identification */
    if (iir == IIR_RDA)     /* Receive Data Available */
    {
        /* Receive Data Available */
        uart->rx_buffer[uart->save_index] = LPC_UART->RBR;
        level = rt_hw_interrupt_disable();
        uart->save_index ++;
        if (uart->save_index >= RT_UART_RX_BUFFER_SIZE)
        {
            uart->save_index = 0;
        }
        rt_hw_interrupt_enable(level);
        /* invoke callback */
        if(uart->parent.rx_indicate != RT_NULL)
        {
            rt_size_t length;
            if (uart->read_index > uart->save_index)
            {
                length = RT_UART_RX_BUFFER_SIZE - uart->read_index + uart->save_index;
            }
            else
            {
                length = uart->save_index - uart->read_index;
            }
            uart->parent.rx_indicate(&uart->parent, length);
        }
    }
    return;
}
static rt_err_t rt_uart2_init (rt_device_t dev)//static
{
    rt_uint32_t Fdiv;
    rt_uint32_t pclkdiv, pclk;
    /* Init UART Hardware */
    if ((LPC_UART_TypeDef*)LPC_UART == LPC_UART2)
    {
        LPC_SC->PCONP |= 1<<24; //默认是关闭的
        LPC_PINCON->PINSEL0 &= ~0x00F00000;
        LPC_PINCON->PINSEL0 |= 0x00500000;  /* Enable TxD2 P0.10, RxD2 P0.11 */
        /* By default, the PCLKSELx value is zero, thus, the PCLK for
        all the peripherals is 1/4 of the SystemFrequency. */
        /* Bit 8,9 are for UART2 */
        pclkdiv = (LPC_SC->PCLKSEL1 >> 16) & 0x03;
        switch ( pclkdiv )
        {
        case 0x00:
        default:
            pclk = SystemCoreClock/4;
            break;
        case 0x01:
            pclk = SystemCoreClock;
            break;
        case 0x02:
            pclk = SystemCoreClock/2;
            break;
        case 0x03:
            pclk = SystemCoreClock/8;
            break;
        }
        LPC_UART2->LCR = 0x83;      /* 8 bits, no Parity, 1 Stop bit */
        Fdiv = ( pclk / 16 ) / UART_BAUDRATE ;  /*baud rate */
        LPC_UART2->DLM = Fdiv / 256;
        LPC_UART2->DLL = Fdiv % 256;
        LPC_UART2->LCR = 0x03;      /* DLAB = 0 */
        LPC_UART2->FCR = 0x07;      /* Enable and reset TX and RX FIFO. */
    }
    /* Ensure a clean start, no data in either TX or RX FIFO. */
    while (( LPC_UART->LSR & (LSR_THRE|LSR_TEMT)) != (LSR_THRE|LSR_TEMT) )
    {
        ;
    }
    while ( LPC_UART->LSR & LSR_RDR )
    {
        Fdiv = LPC_UART->RBR;   /* Dump data from RX FIFO */
    }
    LPC_UART->IER = IER_RBR | IER_THRE | IER_RLS;   /* Enable UART interrupt */
    return RT_EOK;
}
static rt_err_t rt_uart_open(rt_device_t dev, rt_uint16_t oflag)//static
{
    RT_ASSERT(dev != RT_NULL);
    if (dev->flag & RT_DEVICE_FLAG_INT_RX)
    {
        /* Enable the UART Interrupt */
        NVIC_EnableIRQ(UART_IRQn);
    }
    return RT_EOK;
}
static rt_err_t rt_uart_close(rt_device_t dev)//static
{
    RT_ASSERT(dev != RT_NULL);
    if (dev->flag & RT_DEVICE_FLAG_INT_RX)
    {
        /* Disable the UART Interrupt */
        NVIC_DisableIRQ(UART_IRQn);
    }
    return RT_EOK;
}
static rt_size_t rt_uart_read(rt_device_t dev, rt_off_t pos, void* buffer, rt_size_t size)//static
{
    rt_uint8_t* ptr;
    struct rt_uart_lpc *uart = (struct rt_uart_lpc*)dev;
    RT_ASSERT(uart != RT_NULL);
    /* point to buffer */
    ptr = (rt_uint8_t*) buffer;
    if (dev->flag & RT_DEVICE_FLAG_INT_RX)
    {
        while (size)
        {
            /* interrupt receive */
            rt_base_t level;
            /* disable interrupt */
            level = rt_hw_interrupt_disable();
            if (uart->read_index != uart->save_index)
            {
                *ptr = uart->rx_buffer[uart->read_index];
                uart->read_index ++;
                if (uart->read_index >= RT_UART_RX_BUFFER_SIZE)
                {
                    uart->read_index = 0;
                }
            }
            else
            {
                /* no data in rx buffer */
                /* enable interrupt */
                rt_hw_interrupt_enable(level);
                break;
            }
            /* enable interrupt */
            rt_hw_interrupt_enable(level);
            ptr ++;
            size --;
        }
        return (rt_uint32_t)ptr - (rt_uint32_t)buffer;
    }
    return 0;
}
static rt_size_t rt_uart_write(rt_device_t dev, rt_off_t pos, const void* buffer, rt_size_t size)//static
{
    char *ptr;
    ptr = (char*)buffer;
    if (dev->flag & RT_DEVICE_FLAG_STREAM)
    {
        /* stream mode */
        while (size)
        {
            if (*ptr == '\n')
            {
                /* THRE status, contain valid data */
                while ( !(LPC_UART->LSR & LSR_THRE) )
                {
                    ;
                }
                /* write data */
                LPC_UART->THR = '\r';
            }
            /* THRE status, contain valid data */
            while ( !(LPC_UART->LSR & LSR_THRE) )
            {
                ;
            }
            /* write data */
            LPC_UART->THR = *ptr;
            ptr ++;
            size --;
        }
    }
    else
    {
        while ( size != 0 )
        {
            /* THRE status, contain valid data */
            while ( !(LPC_UART->LSR & LSR_THRE) )
            {
                ;
            }
            /* write data */
            LPC_UART->THR = *ptr;
            ptr++;
            size--;
        }
    }
    return (rt_size_t) ptr - (rt_size_t) buffer;
}
void rt_hw_uart2_init(void)
{
    struct rt_uart_lpc* uart;
    /* get uart device */
    uart = &uart2_device;
    /* device initialization */
    uart->parent.type = RT_Device_Class_Char;
    rt_memset(uart->rx_buffer, 0, sizeof(uart->rx_buffer));
    uart->read_index = uart->save_index = 0;
    /* device interface */
    uart->parent.init       = rt_uart2_init;
    uart->parent.open       = rt_uart_open;
    uart->parent.close      = rt_uart_close;
    uart->parent.read       = rt_uart_read;
    uart->parent.write      = rt_uart_write;
    uart->parent.control    = RT_NULL;
    uart->parent.user_data  = RT_NULL;
//  rt_device_register(&uart->parent,
//      "uart0", RT_DEVICE_FLAG_RDWR | RT_DEVICE_FLAG_STREAM | RT_DEVICE_FLAG_INT_RX);
    rt_device_register(&uart->parent,
                       "uart2", RT_DEVICE_FLAG_RDWR | RT_DEVICE_FLAG_STREAM | RT_DEVICE_FLAG_INT_RX | RT_DEVICE_FLAG_DMA_TX);
}
/* 数据到达回调函数*/
rt_err_t uart2_input(rt_device_t dev, rt_size_t size)
{
    rt_sem_release(&rx2_sem);
    return RT_EOK;
}
void uart2_thread_entry(void* parameter)
{
    rt_device_t device;
    rt_uint8_t ch,len=0;
    //初始化信号量
    ch = rt_sem_init(&rx2_sem, "uart2_rx_sem", 0, RT_IPC_FLAG_FIFO);
    //初始化环形缓冲区
//  rt_ringbuffer_init(&uart0_rx_rb,uart0_rx_buffer,256);
    /* 查找系统中的串口0设备 */
    device = rt_device_find("uart2");
    if (device!= RT_NULL)
    {
        /* 设置回调函数及打开设备*/
        rt_device_set_rx_indicate(device, uart2_input);
        rt_device_open(device, RT_DEVICE_OFLAG_RDWR);
    }
    while (1)
    {
        /* 从消息队列中读取消息*/
        if (rt_sem_take(&rx2_sem, RT_WAITING_FOREVER) != RT_EOK)
        {
            continue;
        }
        /* read one character from device */
        while (rt_device_read(device, 0, &ch, 1) == 1)
        {
            uart2_rx_buffer[len] = ch;
            len++;
        }
    }
}
#endif /* end of UART */
/*@}*/
