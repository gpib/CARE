#ifndef __UART0_H__
#define __UART0_H__
void rt_hw_uart0_init(void);
void uart0_thread_entry(void* parameter);
#endif
