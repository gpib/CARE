#ifndef __RT_RING_H__
#define __RT_RING_H__
#include <rtthread.h>
void rb_init(rt_uint8_t *pool, rt_uint16_t size);
rt_bool_t rb_put(const rt_uint8_t *ptr, rt_uint16_t length);
rt_bool_t rb_get(rt_uint8_t *ptr, rt_uint16_t length);
void ring_buf_init(void);
void worker_entry(void* parameter);
#endif
