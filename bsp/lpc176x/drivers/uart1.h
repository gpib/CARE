#ifndef __UART1_H__
#define __UART1_H__
void rt_hw_uart1_init(void);
void uart1_thread_entry(void* parameter);
#endif
