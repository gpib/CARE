#include <rtthread.h>
#include <lwip/sockets.h> /* 使用BSD Socket接口必须包含sockets.h这个头文�?*/
#include "GPIB.h"
#include "lwip/netif.h"
#include "ring.h"
static rt_uint8_t GPIB_connected=0;
static char tcp_rx_buffer[128];
rt_uint8_t tcp_rx_len=0;
void tcpserv(void* parameter)
{
    rt_uint8_t connected;
    char *recv_data; /* 用于接收的指针，后面会做一次动态分配以请求可用内存 */
    rt_uint32_t sin_size,i;
    int sock,  bytes_received;
    struct sockaddr_in server_addr, client_addr;
    rt_bool_t stop = RT_FALSE; /* 停止标志 */
//  rt_ubase_t level;
    rt_uint8_t ch;
    recv_data = rt_malloc(512); /* 分配接收用的数据缓冲 */
    if (recv_data == RT_NULL)
    {
        rt_kprintf("No memory\n");
        return;
    }
    /* 一个socket在使用前，需要预先创建出来，指定SOCK_STREAM为TCP的socket */
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        /* 创建失败的错误处�?*/
        rt_kprintf("Socket error\n");
        /* 释放已分配的接收缓冲 */
        rt_free(recv_data);
        return;
    }
    /* 初始化服务端地址 */
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(TCP_SERVER_PORT); /* 服务端工作的端口 */
    server_addr.sin_addr.s_addr = INADDR_ANY;
    rt_memset(&(server_addr.sin_zero),8, sizeof(server_addr.sin_zero));
    /* 绑定socket到服务端地址 */
    if (bind(sock, (struct sockaddr *)&server_addr, sizeof(struct sockaddr)) == -1)
    {
        /* 绑定失败 */
        rt_kprintf("Unable to bind\n");
        /* 释放已分配的接收缓冲 */
        rt_free(recv_data);
        return;
    }
    /* 在socket上进行监�?*/
    if (listen(sock, 5) == -1)
    {
        rt_kprintf("Listen error\n");
        /* release recv buffer */
        rt_free(recv_data);
        return;
    }
    while(stop != RT_TRUE)
    {
        sin_size = sizeof(struct sockaddr_in);
        /* 接受一个客户端连接socket的请求，这个函数调用是阻塞式�?*/
        connected = accept(sock, (struct sockaddr *)&client_addr, &sin_size);
        GPIB_connected = connected;
        /* 客户端连接的处理 */
        while (1)
        {
            bytes_received = recv(connected,recv_data, 512, 0);
            if (bytes_received <= 0)
            {
                /* 接收失败，关闭这个connected socket */
                lwip_close(connected);
                break;
            }
//                  level = rt_hw_interrupt_disable();
            for(i=0; i<bytes_received; i++)
            {
                ch = recv_data[i];
                tcp_rx_buffer[tcp_rx_len] = ch;
                tcp_rx_len++;
                if(tcp_rx_buffer[0] == 0x08)
                {
                    if (tcp_rx_len == tcp_rx_buffer[2] + 3)         //08 16 07 AA 00 31 32 33 34 35
                    {
                        if (care_protocol_state())
                        {
                            care_protocol(2,tcp_rx_buffer,tcp_rx_len);    //GPIB
                        }
                        memset(tcp_rx_buffer, 0, 128);
                        tcp_rx_len = 0;
                    }
                }
                else if(((ch == 0x0A)||((tcp_rx_buffer[tcp_rx_len-1] == 0x6E)&&(tcp_rx_buffer[tcp_rx_len-2] == 0x5C)))&&(tcp_rx_buffer[0] != 0x08))
                {
                    if (care_protocol_state())
                    {
                        care_protocol(2,tcp_rx_buffer,tcp_rx_len);    //GPIB
                    }
                    //memset(tcp_rx_buffer, 0, 128);
                    tcp_rx_len = 0;
                }
            }
        }
    }
    /* 退出服�?*/
    lwip_close(sock);
    /* 释放接收缓冲 */
    rt_free(recv_data);
    return ;
}
void GPIB_send(char *send_data,rt_uint16_t len)
{
    if(GPIB_connected>0)
    {
        send(GPIB_connected, send_data, len, 0);
    }
}
