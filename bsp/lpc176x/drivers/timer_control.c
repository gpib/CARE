/*
 * 程序清单：动态定时器例程
 *
 * 这个例程会创�?个动态周期型定时器对象，然后控制它进行定时时间长度的更改�?
 */
#include <rtthread.h>
#include "tc_comm.h"
#include "care.h"
#include "GPIB.h"
#include "ds1820.h"
/* 定时器的控制�?*/
static rt_timer_t timer1;
static rt_uint16_t count=0;
static rt_uint8_t pre_sec;
/* 定时器超时函�?*/
static void timeout1(void* parameter)
{
    rt_tick_t timeout = 10;
    if (PIN0_IS_CLR(SW2))
    {
        if (timer_sw_count++ >= 10000)
        {
            timer_sw_count = 0;
        }
    }
    if(LPC_RTC->SEC != pre_sec)
    {
        ms_int = 0;
        pre_sec = LPC_RTC->SEC;
    }
    else
    {
        ms_int += 1;
    }
    if (rand_seed < 0x3E8 )
    {
        rand_seed += 3;
    }
    else
    {
        rand_seed = 0;
    }
    /* 停止定时器自�?*/
    if (count++ >= 2000)
    {
//      TEMP_NOW = ReadTemperature();
//          /* 控制定时器然后更改超时时间长�?*/
        rt_timer_control(timer1, RT_TIMER_CTRL_SET_TIME, (void *)&timeout);
        count = 0;
    }
}
void timer_control_init()
{
    /* 创建定时�? */
    timer1 = rt_timer_create("timer1",  /* 定时器名字是 timer1 */
                             timeout1, /* 超时时回调的处理函数 */
                             RT_NULL, /* 超时函数的入口参�?*/
                             1, /* 定时长度，以OS Tick为单位，�?0个OS Tick */
                             RT_TIMER_FLAG_PERIODIC); /* 周期性定时器 */
    /* 启动定时�?*/
    if (timer1 != RT_NULL)
    {
        rt_timer_start(timer1);
    }
    else
    {
        tc_stat(TC_STAT_END | TC_STAT_FAILED);
    }
}
#ifdef RT_USING_TC
static void _tc_cleanup()
{
    /* 调度器上锁，上锁后，将不再切换到其他线程，仅响应中断 */
    rt_enter_critical();
    /* 删除定时器对�?*/
    rt_timer_delete(timer1);
    timer1 = RT_NULL;
    /* 调度器解�?*/
    rt_exit_critical();
    /* 设置TestCase状�?*/
    tc_done(TC_STAT_PASSED);
}
int _tc_timer_control()
{
   /*设置TestCase清理回调函数 */
    tc_cleanup(_tc_cleanup);
    /* 执行定时器例�?*/
    count = 0;
    timer_control_init();
    /* 返回TestCase运行的最长时�?*/
    return 100;
}
#endif
