/*****************************************************************************
 * $Id$
 *
 * Project: 	NXP LPC1700 Secondary Bootloader Example
 *
 * Description: Provides access to In-Application Programming (IAP) routines
 * 			    contained within the bootROM sector of LPC1100 devices.
 *
 * Copyright(C) 2010, NXP Semiconductor
 * All rights reserved.
 *
 *****************************************************************************
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * products. This software is supplied "AS IS" without any warranties.
 * NXP Semiconductors assumes no responsibility or liability for the
 * use of the software, conveys no license or title under any patent,
 * copyright, or mask work right to the product. NXP Semiconductors
 * reserves the right to make changes in the software without
 * notification. NXP Semiconductors also make no representation or
 * warranty that such application will be suitable for the specified
 * use without further testing or modification.
 *****************************************************************************/

#include "IAP.h"


/* IAP Command Definitions */
#define	IAP_CMD_PREPARE_SECTORS			50
#define	IAP_CMD_COPY_RAM_TO_FLASH		51
#define	IAP_CMD_ERASE_SECTORS			52
#define	IAP_CMD_BLANK_CHECK_SECTORS		53
#define	IAP_CMD_READ_PART_ID			54
#define	IAP_CMD_READ_BOOT_ROM_VERSION	55
#define	IAP_CMD_COMPARE					56
#define	IAP_CMD_REINVOKE_ISP			57
#define IAP_CMD_READ_SERIAL_NUMBER		58

/* IAP boot ROM location and access function */
#define IAP_ROM_LOCATION				0x1FFF1FF1UL
#define IAP_EXECUTE_CMD(a, b)			((void (*)())(IAP_ROM_LOCATION))(a, b)
	
#define MAX_SAVE_LEN 128


rt_uint8_t save[MAX_SAVE_LEN];
	

/*****************************************************************************
** Function name:	u32IAP_PrepareSectors
**
** Description:		Prepares sector(s) for erasing or write operations. This
** 					command must be executed before executing the "Copy RAM to
** 					Flash" or "Erase Sector(s)" commands.
**
** Parameters:		u32StartSector - Number of first sector to prepare.
** 					u32EndSector - Number of last sector to prepare.
**
** Returned value:	Status code returned by IAP ROM function.
**
******************************************************************************/
rt_uint32_t u32IAP_PrepareSectors(rt_uint32_t u32StartSector, rt_uint32_t u32EndSector)
{
	rt_uint32_t u32Status;																						 
	rt_uint32_t au32Result[5];
	rt_uint32_t au32Command[5];

	if (u32EndSector < u32StartSector)
	{
		u32Status = IAP_STA_INVALD_PARAM;
	}
	else
	{
		au32Command[0] = IAP_CMD_PREPARE_SECTORS;
		au32Command[1] = u32StartSector;
		au32Command[2] = u32EndSector;

		IAP_EXECUTE_CMD(au32Command, au32Result);

		u32Status = au32Result[0];
	}
	return u32Status;
}

/*****************************************************************************
** Function name:	u32IAP_CopyRAMToFlash
**
** Description:		Program the flash memory with data stored in RAM.
**
** Parameters:	   	u32DstAddr - Destination Flash address, should be a 256
**                               byte boundary.
**			 		u32SrcAddr - Source RAM address, should be a word boundary
**			 		u32Len     - Number of 8-bit bytes to write, must be 256
**								 512, 1024, or 4096.
*
** Returned value:	Status code returned by IAP ROM function.
**
******************************************************************************/
rt_uint32_t u32IAP_CopyRAMToFlash(rt_uint32_t u32DstAddr, rt_uint32_t u32SrcAddr, rt_uint32_t u32Len)
{
	rt_uint32_t au32Result[5];
	rt_uint32_t au32Command[5];

	au32Command[0] = IAP_CMD_COPY_RAM_TO_FLASH;
	au32Command[1] = u32DstAddr;
	au32Command[2] = u32SrcAddr;
	au32Command[3] = u32Len;
	au32Command[4] = SystemCoreClock / 1000UL;	/* Core clock frequency in kHz */

	IAP_EXECUTE_CMD(au32Command, au32Result);

	return au32Result[0];
}

/*****************************************************************************
** Function name:	u32IAP_EraseSectors
**
** Description:		Erase a sector or multiple sectors of on-chip Flash memory.
**
** Parameters:		u32StartSector - Number of first sector to erase.
** 					u32EndSector - Number of last sector to erase.
*
** Returned value:	Status code returned by IAP ROM function.
**
******************************************************************************/
rt_uint32_t u32IAP_EraseSectors(rt_uint32_t u32StartSector, rt_uint32_t u32EndSector)
{
	rt_uint32_t u32Status;
	rt_uint32_t au32Result[5];
	rt_uint32_t au32Command[5];

	if (u32EndSector < u32StartSector)
	{
		u32Status = IAP_STA_INVALD_PARAM;
	}
	else
	{
		au32Command[0] = IAP_CMD_ERASE_SECTORS;
		au32Command[1] = u32StartSector;
		au32Command[2] = u32EndSector;
		au32Command[3] = SystemCoreClock / 1000UL;	/* Core clock frequency in kHz */

		IAP_EXECUTE_CMD(au32Command, au32Result);

		u32Status = au32Result[0];
	}
	return u32Status;
}

/*****************************************************************************
** Function name:	u32IAP_BlankCheckSectors
**
** Description:		Blank check a sector or multiple sectors of on-chip flash
** 					memory.
**
** Parameters:		u32StartSector - Number of first sector to check.
** 					u32EndSector - Number of last sector to check.
** 					pu32Result[0] - Offset of the first non blank word location
**                  if the Status Code is IAP_STA_SECTOR_NOT_BLANK.
** 					pu32Result[1] - Contents of non blank word location.
**
** Returned value:	Status code returned by IAP ROM function.
**
******************************************************************************/
rt_uint32_t u32IAP_BlankCheckSectors(rt_uint32_t u32StartSector, rt_uint32_t u32EndSector, rt_uint32_t *pu32Result)
{
	rt_uint32_t u32Status;
	rt_uint32_t au32Result[5];
	rt_uint32_t au32Command[5];

	if (u32EndSector < u32StartSector)
	{
		u32Status = IAP_STA_INVALD_PARAM;
	}
	else
	{
		au32Command[0] = IAP_CMD_BLANK_CHECK_SECTORS;
		au32Command[1] = u32StartSector;
		au32Command[2] = u32EndSector;

		IAP_EXECUTE_CMD(au32Command, au32Result);

		if (au32Result[0] == IAP_STA_SECTOR_NOT_BLANK)
		{
			*pu32Result       = au32Result[0];
			*(pu32Result + 1) = au32Result[1];
		}
		u32Status = au32Result[0];
	}
	return u32Status;
}

/*****************************************************************************
** Function name:	u32IAP_ReadPartID
**
** Description:		Read the part identification number.
**
** Parameters:		pu32PartID - Pointer to storage for part ID number.
*
** Returned value:	Status code returned by IAP ROM function.
**
******************************************************************************/
rt_uint32_t u32IAP_ReadPartID(rt_uint32_t *pu32PartID)
{
	rt_uint32_t au32Result[5];
	rt_uint32_t au32Command[5];

	au32Command[0] = IAP_CMD_READ_PART_ID;

	IAP_EXECUTE_CMD(au32Command, au32Result);

	*pu32PartID = au32Result[1];

	return au32Result[0];
}

/*****************************************************************************
** Function name:	u32IAP_ReadBootVersion
**
** Description:		Read the boot code version number.
**
** Parameters:		pu32Major - Major version number in ASCII format.
** 					pu32Minor - Minor version number in ASCII format.
**
** Returned value:	Status code returned by IAP ROM function.
**
******************************************************************************/
rt_uint32_t u32IAP_ReadBootVersion(rt_uint32_t *pu32Major, rt_uint32_t *pu32Minor)
{
	rt_uint32_t au32Result[5];
	rt_uint32_t au32Command[5];

	au32Command[0] = IAP_CMD_READ_BOOT_ROM_VERSION;

	IAP_EXECUTE_CMD(au32Command, au32Result);

	*pu32Major = (au32Result[1] & 0x0000FF00UL) >> 8;
	*pu32Minor = au32Result[1] & 0x000000FFUL;

	return au32Result[0];
}

/*****************************************************************************
** Function name:	u32IAP_ReadBootVersion
**
** Description:		Read the boot code version number.
**
** Parameters:		pu32Major - Major version number in ASCII format.
** 					pu32Minor - Minor version number in ASCII format.
**
** Returned value:	Status code returned by IAP ROM function.
**
******************************************************************************/
void u32IAP_ReadSerialNumber(rt_uint32_t *pu32byte0, rt_uint32_t *pu32byte1,
								 rt_uint32_t *pu32byte2, rt_uint32_t *pu32byte3)
{
	rt_uint32_t au32Result[5];
	rt_uint32_t au32Command[5];

	au32Command[0] = IAP_CMD_READ_SERIAL_NUMBER;

	IAP_EXECUTE_CMD(au32Command, au32Result);

	*pu32byte0 = au32Result[1];
	*pu32byte1 = au32Result[2];
	*pu32byte2 = au32Result[3];
	*pu32byte3 = au32Result[4];
	//au32Result[0] = IAP_STA_CMD_SUCCESS
	return;
}

/*****************************************************************************
** Function name:	u32IAP_Compare
**
** Description:		Compares the memory contents at two locations.
**
** Parameters:		u32Len - Number of bytes to compare, must be a multiple of 4.
**					pu32Offset - Offset of the first mismatch if the Status Code is COMPARE_ERROR
**
** Returned value:	Status code returned by IAP ROM function.
**
******************************************************************************/
rt_uint32_t u32IAP_Compare(rt_uint32_t u32DstAddr, rt_uint32_t u32SrcAddr, rt_uint32_t u32Len, rt_uint32_t *pu32Offset)
{
	rt_uint32_t au32Result[5];
	rt_uint32_t au32Command[5];

	au32Command[0] = IAP_CMD_COMPARE;
	au32Command[1] = u32DstAddr;
	au32Command[2] = u32SrcAddr;
	au32Command[3] = u32Len;

	IAP_EXECUTE_CMD(au32Command, au32Result);

	if (au32Result[0] == IAP_STA_COMPARE_ERROR)
	{
		if (pu32Offset != 0)
		{
			*pu32Offset = au32Result[1];
		}
	}
	return au32Result[0];
}

/*****************************************************************************
** Function name:	vIAP_ReinvokeISP
**
** Description:		Invoke the bootloader in ISP mode.
**
** Parameters:		None.
*
** Returned value:	None.
**
******************************************************************************/
void vIAP_ReinvokeISP(void)
{
	rt_uint32_t au32Result[5];
	rt_uint32_t au32Command[5];

	au32Command[0] = IAP_CMD_REINVOKE_ISP;

	IAP_EXECUTE_CMD(au32Command, au32Result);
}

rt_uint8_t load_image(uint8_t *data, rt_uint16_t length)
{
	if(length > 0){
		
		/*	Prepare Sectors to be flashed */
		if(u32IAP_PrepareSectors(START_SECTOR, END_SECTOR) == IAP_STA_CMD_SUCCESS){
	   		
			/*	Copy data (already) located in RAM to flash */
			if(u32IAP_CopyRAMToFlash(IMG_START_SECTOR, (uint32_t)data, length) == IAP_STA_CMD_SUCCESS){
									   
				/*	Verify the flash contents with the contents in RAM */
				if(u32IAP_Compare(IMG_START_SECTOR, (uint32_t)data, length, 0) == IAP_STA_CMD_SUCCESS){
					
					return 1;
				}
			}
		}
		/*	Error in the IAP functions */
		return 0;

	}else{
		return 0;
	}
}		
static rt_uint8_t au8RxBuffer[1024]  __attribute__ ((aligned(4)));	

void ReadFlash(void)
{
	rt_uint16_t	i;
  unsigned short *bitmap_ptr = (unsigned short *)IMG_START_SECTOR;
	

	for(i=0;i<=MAX_SAVE_LEN-1;i++)
	{
		save[i]=0;
	}
	
	for(i=0;i<=MAX_SAVE_LEN-1;i++)
	{
		save[i] = *(bitmap_ptr+i);  
	}

	if(save[64]==0x7F)
	{
		USB_GPIB_PORT = save[0];
		WIFI_GPIB_PORT = save[1];
		LAN_GPIB_PORT = save[2];
		
		RT_LWIP_IPADDR0 = save[3];
		RT_LWIP_IPADDR1 = save[4];
		RT_LWIP_IPADDR2 = save[5];
		RT_LWIP_IPADDR3 = save[6];
		
		RT_LWIP_GWADDR0 = save[7];
		RT_LWIP_GWADDR1 = save[8];
		RT_LWIP_GWADDR2 = save[9];
		RT_LWIP_GWADDR3 = save[10];
		
		RT_LWIP_MSKADDR0 = save[11];
		RT_LWIP_MSKADDR1 = save[12];
		RT_LWIP_MSKADDR2 = save[13];
		RT_LWIP_MSKADDR3 = save[14];	
		
		TCP_SERVER_PORT = (save[15]<<8) + save[16];
		UDP_SERVER_PORT = (save[17]<<8) + save[18];		
		BOOL_DHCP = save[19]&0x01;
		
		UART0_BAUDRATE = (save[20]<<16) + (save[21]<<8) + save[22];
		UART1_BAUDRATE = (save[23]<<16) + (save[24]<<8) + save[25];
		
		UART_Auto_Mode = save[26]&0x01;//uart0 uart1自动转发模式
		
		GPIB_WAIT_TIME = (int)((save[27]<<24)|(save[28]<<16)|(save[29]<<8)|(save[30]));
		if ((GPIB_WAIT_TIME >=0xFFFFFF)||(GPIB_WAIT_TIME == 0)) GPIB_WAIT_TIME = GPIB_WAIT_TIME_DEFAULT;
	}
	else
	{ 
		BOOL_DHCP = 1;
		USB_GPIB_PORT = inst_address;
		WIFI_GPIB_PORT = inst_address;
		LAN_GPIB_PORT = inst_address;
		
		TCP_SERVER_PORT = DEF_TCP_SERVER_PORT;
		UDP_SERVER_PORT = DEF_UDP_SERVER_PORT;
		
		RT_LWIP_IPADDR0 = 192;
		RT_LWIP_IPADDR1 = 168;
		RT_LWIP_IPADDR2 = 1;
		RT_LWIP_IPADDR3 = 168;
		
		RT_LWIP_GWADDR0 = 192;
		RT_LWIP_GWADDR1 = 168;
		RT_LWIP_GWADDR2 = 1;
		RT_LWIP_GWADDR3 = 1;
		
		RT_LWIP_MSKADDR0 = 255;
		RT_LWIP_MSKADDR1 = 255;
		RT_LWIP_MSKADDR2 = 255;
		RT_LWIP_MSKADDR3 = 0;	
		
		UART0_BAUDRATE = UART0_BAUDRATE_DEFAULT;
		UART1_BAUDRATE = UART1_BAUDRATE_DEFAULT;
		
		GPIB_WAIT_TIME = GPIB_WAIT_TIME_DEFAULT;
		
		UART_Auto_Mode = 0;
		
	};
}
void WriteFlash(rt_uint8_t param) 
{		 
  rt_uint8_t i,param_restore;
	
	param_restore = param;
	if(param_restore==0)
	{
		save[0] = USB_GPIB_PORT;
		save[1] = WIFI_GPIB_PORT;
		save[2] = LAN_GPIB_PORT;
		
		save[3] = RT_LWIP_IPADDR0;
		save[4] = RT_LWIP_IPADDR1;
		save[5] = RT_LWIP_IPADDR2;
		save[6] = RT_LWIP_IPADDR3;
		
		save[7] = RT_LWIP_GWADDR0;
		save[8] = RT_LWIP_GWADDR1;
		save[9] = RT_LWIP_GWADDR2;
		save[10] = RT_LWIP_GWADDR3;
		
		save[11] = RT_LWIP_MSKADDR0;
		save[12] = RT_LWIP_MSKADDR1;
		save[13] = RT_LWIP_MSKADDR2;
		save[14] = RT_LWIP_MSKADDR3;
		
		save[15] = (TCP_SERVER_PORT>>8)&0xFF;
		save[16] = (TCP_SERVER_PORT)&0xFF;
		save[17] = (UDP_SERVER_PORT>>8)&0xFF;
		save[18] = (UDP_SERVER_PORT)&0xFF;
		
		save[19] = BOOL_DHCP;
		
		save[20] = (UART0_BAUDRATE>>16)&0xFF;
		save[21] = (UART0_BAUDRATE>>8)&0xFF;
		save[22] = (UART0_BAUDRATE)&0xFF;
		
		save[23] = (UART1_BAUDRATE>>16)&0xFF;
		save[24] = (UART1_BAUDRATE>>8)&0xFF;
		save[25] = (UART1_BAUDRATE)&0xFF;
		
		save[26] = 0;//uart0 uart1自动转发模式
		
		save[27] = (GPIB_WAIT_TIME>>24)&0xFF;
		save[28] = (GPIB_WAIT_TIME>>16)&0xFF;
		save[29] = (GPIB_WAIT_TIME>>8)&0xFF;
		save[30] = (GPIB_WAIT_TIME)&0xFF;

		save[64] = 0x7F;//标志
	}
	else if(param_restore==1)
	{
		for(i=0;i<=MAX_SAVE_LEN-1;i++)
		{
			save[i] = 0xFF;	
		}		
	}
	for(i=0;i<=MAX_SAVE_LEN-1;i++)
	{
			au8RxBuffer[2*i] = save[i];	
	}
	if((u32IAP_PrepareSectors(START_SECTOR, END_SECTOR)==IAP_STA_CMD_SUCCESS)&&(u32IAP_EraseSectors(START_SECTOR,END_SECTOR)==IAP_STA_CMD_SUCCESS)) 
	{
		load_image(au8RxBuffer,1024);
	}
}
/*****************************************************************************
 **                            End Of File
 *****************************************************************************/
